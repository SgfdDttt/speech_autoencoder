# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4
import sys
import time
import os
import math
import argparse
import random
import torch
import torch.nn as nn
from data_loader import Dataset
from Models import Encoder,Decoder,FeedforwardDecoder,Autoencoder,custom_init
from torch.autograd import Variable
from torch import optim
import torch.nn.functional as F

fixed_seed=24011993
random.seed(fixed_seed)
torch.manual_seed(fixed_seed)

# argument parser
parser = argparse.ArgumentParser()
parser.add_argument("--datafile",
    help="file containing the training data")
parser.add_argument("--maxlength",help="longest possible sequence",type=int,default=None)
parser.add_argument("--batchsize",help="batch size",type=int,default=100)
parser.add_argument("--trainsize",
        help="number of training examples before validation",type=int,default=10000)
parser.add_argument("--validsize",
        help="number of validation examples before validation",type=int,default=1000)
parser.add_argument("--verbose",help="print out some infos",action="store_true")
parser.add_argument("--reload",help="where to reload from",type=str,default=None)
parser.add_argument("--savedir",
    help="directory to save the models into",default="savedir")
parser.add_argument("--gradclip",help="clip gradient",default=50.)
parser.add_argument("--embeddings",
        help="size of positional embeddings",type=int,default=128)
parser.add_argument("--hidden",
        help="number of cells for the decoder and encoder",type=int,default=128)
parser.add_argument("--enclayers",
        help="number of layers for encoder rnn",type=int,default=2)
parser.add_argument("--declayers",
        help="number of layers for decoder rnn",type=int,default=2)
parser.add_argument("--feedforward",
        help="decoder is a feedforward net",action="store_true")
args = parser.parse_args()
print("=== ARGUMENTS ===")
print(args)
print("=================")

# load data
assert args.validsize % args.batchsize == 0
assert args.trainsize % args.batchsize == 0
epsilon=1e-8 # a small quantity
data=torch.load(args.datafile)
dataset=Dataset(data)
print('number of training samples: ' + str(dataset.get_num_samples('train')))
print('number of validation samples: ' + str(dataset.get_num_samples('valid')))
os.system("mkdir -p " + args.savedir.rstrip('/'))

print("init network")
# 1. build network
# encoder
bneck_size=400
encoder=Encoder(dataset.input_dim(),args.hidden, \
        args.enclayers,birnn=True,bottleneck=bneck_size)
# decoder
in_size=args.embeddings+6
if bneck_size is None:
    in_size+=2*args.hidden
else:
    in_size+=2*bneck_size
if args.feedforward:
    decoder=FeedforwardDecoder(in_size, args.embeddings, args.hidden, \
            dataset.input_dim(), args.declayers)
else:
    decoder=Decoder(in_size, args.embeddings, args.hidden, \
            dataset.input_dim(), args.declayers)
model=Autoencoder(encoder,decoder)
max_length=dataset.get_max_length()
if args.maxlength is not None:
    max_length=args.maxlength
model.make_positional_embeddings(max_length,args.embeddings)

if args.reload is not None:
    print('reloading from file ' + args.reload)
    model=torch.load(args.reload)

# optimizer
optimizer = optim.Adam(model.parameters())
criterion=torch.nn.MSELoss(size_average=False,reduce=False)

def forward(num_examples,split=None,backprop=False):
    overall_loss=0
    num_samples=0
    num_batches=num_examples/args.batchsize
    for ii in range(num_batches):
        batch=dataset.get_next_batch(args.batchsize,\
                upper_frame_limit=args.maxlength,split=split)
        sp,sp_mask=batch
        num_samples+=sp_mask.sum()*sp.size(2)
        if args.verbose:
            print("batch "+str(ii+1)+"/"+str(num_batches))
            sys.stdout.flush()
        # forward through model
        optimizer.zero_grad()
        output=model.forward(Variable(sp), sp_mask.byte())
        # compute loss
        # target is next time step
        loss=torch.sum(criterion(output,Variable(sp)))
        overall_loss+=loss.data[0]
        if backprop:
            loss.backward()
            nn.utils.clip_grad_norm(model.parameters(), args.gradclip)
            optimizer.step()
    return overall_loss, num_samples
# end def forward

savefile=args.savedir.rstrip('/')+"/model-step-0"
torch.save(model, savefile)

# train
step=0
while True:
    start_time=time.time()
    if args.verbose:
        print("STEP " + str(step) + " TRAINING")
        sys.stdout.flush()
    # TRAINING
    loss,num_samples=forward(args.trainsize,backprop=True,split='train')
    step+=args.trainsize
    average_training_loss=loss/num_samples
    # VALIDATION
    if args.verbose:
        print("STEP " + str(step) + " VALIDATION")
        sys.stdout.flush()
    loss,num_samples=forward(args.validsize,backprop=False,split='valid')
    average_validation_loss=loss/num_samples
    stop_time=time.time()
    # SAVE MODEL
    savefile=args.savedir.rstrip('/')+"/model-step-"+str(step)
    torch.save(model, savefile)
    # LOG
    print("step " + str(step) + \
        " ; train loss " + str(average_training_loss) + \
        " ; val loss " + str(average_validation_loss) + \
        " ; done in " + str(stop_time-start_time) + "s" + \
        " ; saved model to " + savefile)
