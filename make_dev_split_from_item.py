# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4
import sys
import random
# from item file, make 2 item files:
# one for training, one for dev
# the dev file should contain, per speaker, a 10% sample for each n-gram
# so if there are 1000 2-grams, 100 3-grams and 10 4-grams, dev
# should have 100 2-grams, 10 3-grams and 1 4-gram.
item_file=sys.argv[1]

fixed_seed=24011993
random.seed(fixed_seed) # to always get the same (random) result

file2ngrams={}
counter=0
for line in open(item_file,'r'):
    if line[0]=='#':
        continue
    counter+=1
    if counter % int(1e6) == 0:
        print(counter)
    fname,start,stop,ngram=line.split(' ')[:4]
    n=len(ngram.split(','))
    file2ngrams.setdefault(fname,dict())
    file2ngrams[fname].setdefault(n,list())
    file2ngrams[fname][n].append((start,stop,ngram))

dev,train=[],[]
for fname,ngrams in file2ngrams.iteritems():
    print(fname)
    for _,truc in ngrams.iteritems(): # truc is a list
        # shuffle list
        c_truc=list(truc) # copy
        random.shuffle(c_truc)
        limit=int(round(len(c_truc)*0.1))
        if limit>0:
            dev.extend([tuple([fname])+_c for _c in c_truc[:limit]])
        train.extend([tuple([fname])+_c for _c in c_truc[limit:]])
print(len(dev))
print(len(train))
print(counter)
#assert len(train)+len(dev)==counter

f=open('dev.item','w')
f.write('#file onset offset #ngram speaker\n')
for (fname,start,stop,ngram) in dev:
    f.write(' '.join([fname,start,stop,ngram,fname]).replace('\n','')+'\n')
f.flush(); f.close()
f=open('train.item','w')
f.write('#file onset offset #ngram speaker\n')
for (fname,start,stop,ngram) in train:
    f.write(' '.join([fname,start,stop,ngram,fname]).replace('\n','') +'\n')
f.flush(); f.close()
