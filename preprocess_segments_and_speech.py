# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4
# custom data loader to load speech corpus
import sys
import torch
import glob
import math
import h5features as h5f

savefile=sys.argv[1] #'/export/a12/nholzen/phn2ie_data_large.pt'
NORMALIZE=False#True
epsilon=1e-8 # small quantity


def find_index(value,vector):
    # return index such that vector[index-1]<value<=vector[index]
    left,right=0,len(vector)-1
    index=(left+right)/2
    while right-left>1:
        if vector[index]<value:
            left=index 
        else:
            right=index 
        index=(left+right)/2
    return index

def compute_stats(vectors):
    # vectors is a list of torch tensors
    # we just normalize everything without speaker id
    mean=torch.zeros(vectors[0].size(1))
    stddev=torch.zeros(vectors[0].size(1))
    for v in vectors:
        mean+=v.sum(dim=0)
        stddev+=torch.pow(v,2).sum(dim=0)
    num_items=sum(v.size(0) for v in vectors)
    mean/=num_items
    stddev/=num_items
    stddev-=torch.pow(mean, 2)
    stddev=torch.pow(stddev, 0.5)
    return mean, stddev

def normalize(vectors, mean, stddev):
    # vectors is a list of torch tensors
    stddev_reg=stddev+epsilon
    for ii in range(len(vectors)):
        vectors[ii]=torch.add(vectors[ii],-1,mean)
        vectors[ii]=torch.div(vectors[ii],stddev_reg)

def load_speech(speech_file):
    # speech_file is name of h5feature file
    # output is dict filename -> features,timestamps
    speech_data={}
    rdata = h5f.Reader(speech_file).read()
    speech_data=rdata.dict_features()
    timestamps=rdata.dict_labels()
    for _k in speech_data:
        speech_data[_k]=torch.from_numpy(speech_data[_k]).type(torch.FloatTensor)
    return speech_data,timestamps

def remove_ind_1(speech_data):
    _keys=list(speech_data.keys())
    for _k in _keys:
        speech_data[_k]=speech_data[_k][:,1:]

def load_segments(segment_file,speech_data,timestamps):
    segments=[]
    total=0
    for line in open(segment_file,'r'):
        if '#' in line:
            continue
        total+=1
        fname,start,stop,transcription,_ = line.strip('\n').split(' ')
        if fname not in speech_data:
            continue
        start_ind=find_index(float(start),timestamps[fname])
        stop_ind=find_index(float(stop),timestamps[fname])
        if speech_data[fname][start_ind:stop_ind+1].size(0)==0:
            print('ARGH')
            continue
        segments.append((fname,start_ind,stop_ind+1,transcription))
    print("segment file " + segment_file + ", # segments in file " \
            + str(total) + ", # of segments kept " + str(len(segments)))
    return segments 
# end def load_segments

def make_data(speech_file, train_item, valid_item):
    print('speech file: ' + speech_file)
    sp_data,timestamps=load_speech(speech_file)
    print('train')
    train_segs=load_segments(train_item,sp_data,timestamps)
    print('valid')
    valid_segs=load_segments(valid_item,sp_data,timestamps)
    print('Saving data to \''+savefile+'\'...')
    save_data = {'train': train_segs, 'valid': valid_segs, 'src': sp_data, 'timestamps': timestamps}
    torch.save(save_data, savefile)

speech_datafile="/scratch2/nholzenb/xitsonga/1hot.h5" #"/scratch2/jkaradayi/share/features_ZR2015/Buckeye_1hot.h5"#"/scratch2/jkaradayi/share/features_ZR2015/xitsonga_1hot.h5" #"/scratch2/rriad/projects/interspeech_paper_abnet3/features/xitsonga_fb40_mvn_vad_allchannels.h5f" #"/scratch2/rriad/projects/interspeech_paper_abnet3/features/buckeye_fb40_mvn_vad_allchannels.h5f"
train_item="/scratch2/nholzenb/xitsonga/xitsonga_2_to_12_grams.train.item" #"/scratch2/nholzenb/xitsonga/xitsonga_abx_eval_words.item" #"/scratch2/nholzenb/buckeye/buckeye_2_to_12_grams.train.item"
valid_item="/scratch2/nholzenb/xitsonga/xitsonga_2_to_12_grams.dev.item" #"/scratch2/nholzenb/xitsonga/xitsonga_abx_eval_words.item" #"/scratch2/nholzenb/buckeye/buckeye_2_to_12_grams.dev.item"

make_data(speech_datafile, train_item, valid_item)
