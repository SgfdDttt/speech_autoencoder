# !/bin/bash
# Compute distances, score and analyze results given features and a task file

if [ $# -lt 3 ]
then
	echo "Missing arguments"
	exit 0
fi

# arguments
task_file=$1 # a .abx file
feature_file=$2 # a h5features file
out_dir=$3 # folder that files get written to, can get large

# temporary files
distance_file=$out_dir/abx.distances
score_file=$out_dir/abx.score
analyze_file=$out_dir/abx.csv

# print every command that is executed
set -x

start=`date`
echo started on $start

# 0. make sure there is nothing in the out folder
mkdir -p $out_dir
rm -r $out_dir/*

# 1. compute distances
python distance.py $feature_file $task_file $distance_file -j 1 -n 0

# 2. score distances
python score.py $task_file $distance_file $score_file

# 3. analyze results
python analyze.py $score_file $task_file $analyze_file

# 4. print mean abx score
python compute_abx_error.py $analyze_file

# delete files (optional)

stop=`date`
echo ended on $stop
