# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4
import sys
# read segmentation from segmentation_file
# extract ngrams of length min_n to max_n,
# not going across boundaries (marked by SIL)
segmentation_file=sys.argv[1]
min_n=2 #int(sys.argv[2])
max_n=12 #int(sys.argv[3])
SIL='SIL'
SPN='SPN'
file2segments=dict()
# one segment is (start,stop,label)
for line in open(segmentation_file,'r'):
    fname=line.strip('\n').split(' ')[0]
    stuff=tuple(line.strip('\n').split(' ')[1:])
    file2segments.setdefault(fname,[])
    file2segments[fname].append(stuff)

all_segments=[] # list of fname, start, stop, label
                # label is comma-separated phones
for fname,segments in file2segments.iteritems():
    for ii in range(len(segments)):
        for jj in range(min_n,max_n+1):
            if not ii+jj-1<len(segments): # can't go beyond boundary of file
                continue
            if SPN in set(_c[2] for _c in segments[ii:ii+jj]): # can't go across speech noises
                continue
            if SIL in set(_c[2] for _c in segments[ii:ii+jj]): # can't go across silences
                continue
            _start=segments[ii][0]
            _stop=segments[ii+jj-1][1]
            _label=','.join(_c[2] for _c in segments[ii:ii+jj])
            all_segments.append(tuple([fname,_start,_stop,_label]))

print("#file onset offset #ngram speaker")
for fname,_start,_stop,_label in all_segments:
    print(fname + ' ' + _start + ' ' + _stop + ' ' + _label + ' ' + fname)
