# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4
# custom data loader to load speech corpus
import random
import torch
from torch.autograd import Variable

class Dataset(object):

    def __init__(self, srcData, cuda=False):
        self.data = srcData # actual data that was loaded as-is from file
        self.cuda = cuda

    def sort_by_length(self,batch):
        # sort batch by decreasing length
        lengths=torch.FloatTensor([c.size(0) for c in batch])
        _,new_order=torch.sort(lengths)
        new_batch=[]
        for ind in new_order:
            new_batch.insert(0,batch[ind])
        return new_batch, new_order.tolist()

    def get_all_segments(self,split='train'):
        out=[]
        for fname,start_ind,stop_ind,transcription in self.data[split]:
            speech=self.data['src'][fname][start_ind:stop_ind,:]
            times=(self.data['timestamps'][fname][start_ind], \
                    self.data['timestamps'][fname][stop_ind])
            out.append((speech,fname,times,transcription))
        return out

    def pad_batch(self,tmp):
        # pad
        length=max(x.size(0) for x in tmp)
        batch,mask=[],[]
        dim=tmp[0].size(1)
        for _b in tmp:
            diff=length-_b.size(0)
            batch.append(
                    torch.cat([
                        _b,
                        torch.zeros(diff,dim)
                        ],dim=0)
                    )
            mask.append(
                    torch.cat([
                        torch.ones(_b.size(0)),
                        torch.zeros(diff)
                        ],dim=0)
                    )
        batch=torch.stack(batch,dim=0).contiguous()
        mask=torch.stack(mask,dim=0).contiguous()
        return batch,mask

    def get_next_batch(self, batch_size, upper_frame_limit=None, split='train'):
        # randomly sample batch_size elements from the item file
        batch=[]
        while len(batch)<batch_size:
            rand_ind=int(random.random()*len(self.data[split]))
            fname,start_ind,stop_ind,transcription=self.data[split][rand_ind]
            if upper_frame_limit is not None:
                if stop_ind-start_ind>upper_frame_limit:
                    continue
            sample=self.data['src'][fname][start_ind:stop_ind,:]
            batch.append(sample)
        # sort in order of decreasing length to use pack_padded_sequence later
        tmp,_=self.sort_by_length(batch)
        batch,mask=self.pad_batch(tmp)
        return batch, mask

    def input_dim(self):
        _k=self.data['src'].keys()[0]
        return self.data['src'][_k].size(1)

    def get_max_length(self):
        x=max(stop-start for _,start,stop,_ in self.data['train'])
        y=max(stop-start for _,start,stop,_ in self.data['valid'])
        return max(x,y)

    def get_num_samples(self,split):
        return len(self.data[split])
