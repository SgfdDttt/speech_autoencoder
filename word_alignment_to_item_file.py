# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4
import sys
import random

# constants
fixed_seed=24011993
random.seed(fixed_seed)
forbidden=['SIL', 'SPN']
upper_limit_occurrences=20
file_name=sys.argv[1]
BUCKEYE=False
XITSONGA=True
assert (BUCKEYE or XITSONGA) and (not (BUCKEYE and XITSONGA))

def right_duration(start_time, stop_time):
    # cheks whether word is between 0.3 and 1.5 seconds long
    duration=float(stop_time)-float(start_time)
    return (duration >= 0.3) and (duration <= 1.5)

# return triplets of start time, stop time, word
segments=list()
for line in open(file_name, 'r'):
    # make tuples of (filename, start, stop, word)
    filename,start,stop,word=line.strip('\n').strip(' ').split(' ')
    if (word not in forbidden) and right_duration(start,stop):
        segments.append((filename,start,stop,word))

word_counts = dict()
for _segment in segments:
    word_counts.setdefault(_segment[3],0)
    word_counts[_segment[3]]+=1

for word in word_counts: # get rid of hapaxes and reduce count to a max
    if word_counts[word] < 2:
        word_counts[word] = 0
    word_counts[word] = min(word_counts[word],upper_limit_occurrences)

# print out to file
print('#file onset offset #speaker word')
random.shuffle(segments) # shuffle to pick words randomly
for _segment in segments:
    fname,start,stop,word=_segment
    if (word_counts[word] > 0):
        if BUCKEYE:
            speaker=fname[1:3]
        elif XITSONGA:
            speaker=fname.split('_')[-2]
        print(' '.join((fname,start,stop,speaker,word)))
        word_counts[word] -= 1
assert sum(_n for _,_n in word_counts.items())==0
