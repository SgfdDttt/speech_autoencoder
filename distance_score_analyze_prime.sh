# !/bin/bash
# Compute distances, score and analyze results given features and a task file

if [ $# -lt 3 ]
then
	echo "Missing arguments"
	exit 0
fi

source /home/nholzenb/.bashrc

abxpy_dir=/home/nholzenb/ABXpy/ABXpy
cd $abxpy_dir

# arguments
task_file=$1 # a .abx file
feature_file=$2 # a h5features file
out_dir=$3 # folder that files get written to, can get large

# temporary files
distance_file=$out_dir/abx.distances
score_file=$out_dir/abx.score
analyze_file=$out_dir/abx.csv

start=`date`
echo started on $start

# print every command that is executed
set -x

# 0. make sure there is nothing in the out folder
mkdir -p $out_dir
rm -r $out_dir/*

# 1. compute distances
python $abxpy_dir/distance.py $feature_file $task_file $distance_file -d euclidean.euclidean_distance -j 1

# 2. score distances
python $abxpy_dir/score.py $task_file $distance_file $score_file

# 3. analyze results
python $abxpy_dir/analyze.py $score_file $task_file $analyze_file

# 4. print mean abx score
python $abxpy_dir/mean_abx_score.py $analyze_file

# delete files (optional)

stop=`date`
echo ended on $stop
