# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4
import sys

bstep=None
bloss=None
for line in open(sys.argv[1],'r'):
    if ";" not in line:
        continue
    trucs=line.strip('\n').split(';')
    val_loss=float(trucs[2].strip(' ').split(' ')[2])
    ep=trucs[0].split(' ')[1]
    if bstep is None:
        bstep=ep
        bloss=val_loss
    elif bloss>val_loss:
        bstep=ep
        bloss=val_loss
print(bstep,bloss,';',ep,val_loss)
