# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4
import glob


datafile="/scratch2/jkaradayi/data/challenge_datasets_Aren_features/english_ali2.txt"
datadir="/scratch2/jkaradayi/data/challenge_datasets_Aren_features/english/"

num_words={}
avg_length={}
stddev_length={}
total_avg,total_stddev,total_num=0,0,0
relevant_files=set()

for thing in glob.glob(datadir.rstrip('/')+'/*.feat'):
    relevant_files.add(thing.split('/')[-1].split('.')[0])

for line in open(datafile,'r'):
    fname,start,stop,wordid=line.strip('\n').split(' ')
    if fname not in relevant_files:
            continue
    num_words.setdefault(fname,0)
    num_words[fname]+=1
    total_num+=1
    avg_length.setdefault(fname,0)
    avg_length[fname]+=float(stop)-float(start)
    total_avg+=float(stop)-float(start)
    stddev_length.setdefault(fname,0)
    stddev_length[fname]+=(float(stop)-float(start))**2
    total_stddev+=(float(stop)-float(start))**2

for key in num_words:
    avg_length[key] /= float(num_words[key])
    stddev_length[key] /= float(num_words[key])
    stddev_length[key] -= avg_length[key]**2
    stddev_length[key] = stddev_length[key]**(0.5)

total_avg /= float(total_num)
total_stddev /= float(total_num)
total_stddev -= total_avg**2
total_stddev = total_stddev**(0.5)

for key in relevant_files:
    out=str(key) + '\t' + str(num_words[key]) + '\t' \
                    + str(avg_length[key]) + '\t' + str(stddev_length[key])
    print(out)

print('total\t' + str(total_num) + '\t' + str(total_avg) + '\t' + str(total_stddev))

def distance(fname):
    out=(avg_length[fname]-total_avg)**2 + (stddev_length[fname]-total_stddev)**2
    return out

dev_files=set()
while sum(num_words[fname] for fname in dev_files)<0.1*total_num:
    # find fname closest to stats
    best_fname=None
    for key in relevant_files:
        if best_fname is None:
            best_fname=key
        else:
            if distance(fname)>distance(key):
                best_fname=key
    dev_files.add(best_fname)
    relevant_files.remove(best_fname)
print(dev_files)
print(sum(num_words[fname] for fname in dev_files))
