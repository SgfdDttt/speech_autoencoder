# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4
import sys
import os
import numpy as np
import h5features

segmentation_file=sys.argv[1]
savefile=sys.argv[2]
offset=0.0125
rate=0.003
#remove=['SIL','SPN'] # these are not phones
remove=[]

def one_hot(hot,size):
    onehot=[1. if ii==hot else 0. for ii in range(size)]
    if sum(onehot)==0.:
        onehot.append(1.)
    else:
        onehot.append(0.)
    assert sum(onehot)==1.
    return onehot

def get_phone_inventory(seg_file):
    phones=set()
    for line in open(seg_file,'r'):
        phones.add(line.strip('\n').strip(' ').split(' ')[3])
    return phones

def make_feats(seg_file,phn2int):
    files,times,features=[],[],[]
    _times,_features=[],[]
    files.append('<sos>')
    for line in open(seg_file,'r'):
        fname,start,stop,phn=line.strip('\n').strip(' ').split(' ')
        if fname!=files[-1]:
            assert len(_times)==len(_features)
            times.append(np.array(_times,dtype=np.float))
            features.append(np.array(_features,dtype=np.float))
            files.append(fname)
            _times=[]
            _features=[]
        # end if
        counter=float(start)
        while counter<float(stop):
            _times.append(counter)
            _features.append(one_hot(phn2int.get(phn,-1),phn2int["l"]))
            counter+=rate
        # end while
    # end for
    # "trailing" file
    assert len(_times)==len(_features)
    times.append(np.array(_times,dtype=np.float))
    features.append(np.array(_features,dtype=np.float))
    assert len(files)==len(times)
    assert len(files)==len(features)
    return files[1:],times[1:],features[1:]


vocab=get_phone_inventory(segmentation_file)
vocab=vocab-set(remove)
vocab=sorted(list(vocab))
print(vocab)
print(len(vocab))
invvocab=dict((w,i) for i,w in enumerate(vocab))
invvocab['l']=len(vocab)
files,times,feats=make_feats(segmentation_file,invvocab)

if os.path.exists(savefile):
    os.remove(savefile)

print("saving to " + savefile)
h5features.write(savefile, 'features', files, times, feats)
