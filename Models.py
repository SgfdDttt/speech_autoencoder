# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4
import sys
import torch
import torch.nn as nn
from torch.autograd import Variable
import math

def custom_init(module):
    for name, param in module.named_parameters():
        if 'bias' in name:
            stddiv=(3./param.size(0))**(0.5)
            nn.init.uniform(param,-stddiv,stddiv)
        elif 'weight' in name:
            nn.init.xavier_uniform(param)

_INF = float('inf')

class Encoder(nn.Module):

    def __init__(self, input_size, hidden_size, num_layers, birnn=False, bottleneck=None):
        self.num_layers = num_layers
        self.num_directions = 2 if birnn else 1
        assert hidden_size % self.num_directions == 0
        self.hidden_size = hidden_size // self.num_directions
        self.input_size = input_size # the size of the speech features
        self.bottleneck=bottleneck if bottleneck is None \
                else bottleneck // self.num_directions

        super(Encoder, self).__init__()
        self.rnn=nn.LSTM(self.input_size, self.hidden_size, \
                num_layers=self.num_layers, bidirectional=birnn, batch_first=True)
        custom_init(self.rnn)
        if self.bottleneck is not None:
            self.bneck_layer=nn.LSTM(hidden_size, self.bottleneck, \
                    num_layers=1, bidirectional=birnn, batch_first=True)
            custom_init(self.bneck_layer)
        else:
            self.bneck_layer=None

    def forward(self, input, lengths=None, hidden=None):
        batch_size = input.size(0) # batch first for multi-gpu compatibility
        if hidden is None:
            h_size = (self.num_layers * self.num_directions, batch_size, self.hidden_size)
            h_0 = Variable(input.data.new(*h_size).zero_(), requires_grad=False)
            c_0 = Variable(input.data.new(*h_size).zero_(), requires_grad=False)
            hidden = (h_0, c_0)
        emb=nn.utils.rnn.pack_padded_sequence(input,lengths,batch_first=True)
        outputs, hidden_t=self.rnn(emb,hidden)
        if hasattr(self,'bneck_layer'):
            if self.bneck_layer is not None:
                h_size = (1 * self.num_directions, batch_size, self.bottleneck)
                h_0 = Variable(input.data.new(*h_size).zero_(), requires_grad=False)
                c_0 = Variable(input.data.new(*h_size).zero_(), requires_grad=False)
                hidden = (h_0, c_0)
                outputs, hidden_t=self.bneck_layer(outputs,hidden)
        outputs,_=nn.utils.rnn.pad_packed_sequence(outputs, batch_first=True)
        return hidden_t, outputs

class FeedforwardDecoder(nn.Module):

    def __init__(self, input_size, embeddings_size, hidden_size, output_size, num_layers):
        self.num_layers = num_layers
        self.emb_size = embeddings_size # size of positional embeddings
        self.hidden_size = hidden_size
        self.input_size = input_size
        self.output_size = output_size
        super(FeedforwardDecoder, self).__init__()

        self.net=[]
        sizes=[self.input_size] + [self.hidden_size]*self.num_layers
        for s1,s2 in zip(sizes[:-1],sizes[1:]):
            self.net.append(nn.Linear(s1,s2))
            self.net.append(nn.ReLU())
        self.net.append(nn.Linear(sizes[-1],self.output_size))
        self.net=nn.Sequential(*self.net)
        custom_init(self.net)

    def forward(self, input, hidden, seq_encoding, mask=None):
        # for now ignore input
        # seq_encoding is batch_size x time x dim
        seq,_=nn.utils.rnn.pad_packed_sequence(seq_encoding, batch_first=True)
        batch_size,time,dim=seq.size()
        inp=seq.contiguous().view(batch_size*time,dim)
        out=self.net(inp)
        out=out.view(batch_size,time,self.output_size)
        return out,None # there are no hidden state but be consistent with API

class Decoder(nn.Module):

    def __init__(self, input_size, embeddings_size, hidden_size, output_size, num_layers):
        self.num_layers = num_layers
        self.emb_size = embeddings_size # size of positional embeddings
        self.hidden_size = hidden_size
        self.input_size = input_size # self.emb_size + 2*self.hidden_size + 6
        self.output_size = output_size
        super(Decoder, self).__init__()

        self.rnn = nn.LSTM(self.input_size,self.hidden_size, \
                self.num_layers,batch_first=True)
        custom_init(self.rnn)
        self.linear_out=nn.Linear(self.hidden_size,self.output_size)
        custom_init(self.linear_out)

    def forward(self, input, hidden, seq_encoding, mask=None):
        # for now ignore input

        outputs,hidden_states=self.rnn(seq_encoding,hidden)
        outputs,_=nn.utils.rnn.pad_packed_sequence(outputs, batch_first=True)
        out=self.linear_out(outputs.contiguous().view(-1, outputs.size(2)))
        out=out.view(outputs.size(0),-1,self.output_size)
        """
        batch_size = input.size(0)

        # n.b. you can increase performance if you compute W_ih * x for all
        # iterations in parallel, but that's only possible if
        # self.input_feed=False
        outputs = []
        h = hidden
        # TODO forward input through decoder
        for i, x_t in enumerate(seq_encoding.split(1)): #chunk(emb.size(0), dim=0)):
            print(x_t.size())
            assert False
            x_t = emb_t.squeeze(0)
            rnn_output, h = self.rnn(torch.cat([emb_t,attn_output],dim=1).unsqueeze(1), h)
            attn_output, attn = self.attn(
                    h[0].transpose(0,1).contiguous().view(batch_size, -1),
                    context
                    )
            outputs += [torch.cat([rnn_output.squeeze(1),attn_output],dim=1)]

        outputs = torch.stack(outputs)
        return outputs.transpose(0, 1), h, attn
        """
        return out,hidden_states
    
class Autoencoder(nn.Module):

    def __init__(self, encoder, decoder):
        super(Autoencoder, self).__init__()
        self.encoder = encoder
        self.decoder = decoder

    def make_init_decoder_output(self, context):
        batch_size = context.size(0)
        h_size = (batch_size, self.decoder.hidden_size)
        return Variable(context.data.new(*h_size).zero_(), requires_grad=False)

    def make_positional_embeddings(self,length,dimension):
        self.pos_beds=nn.Embedding(length+1, # the +1 is because
                                    self.decoder.emb_size, # padding is 0 so
                                    padding_idx=0) # necessary to offset all by 1
        custom_init(self.pos_beds)

    def get_positional_embeddings(self,mask):
        # output has dimension (batch_size x max_length x (dim+6))
        lengths=[int(_x.sum()) for _x in mask]
        _l=Variable(torch.FloatTensor(lengths)).unsqueeze(1)
        max_length=max(lengths)
        positions=Variable(torch.arange(1,max_length+1), requires_grad=False)
        positions=positions.unsqueeze(0).expand(len(lengths),-1)
        rel_positions=torch.stack([
            (positions-1)/_l,
            positions/_l,
            torch.cos(2*math.pi*(positions-1)/_l),
            torch.sin(2*math.pi*(positions-1)/_l),
            torch.cos(2*math.pi*positions/_l),
            torch.sin(2*math.pi*positions/_l)
            ]).permute(1,2,0)
        pos_beds=torch.cat([
            rel_positions,
            self.pos_beds(positions.type(torch.LongTensor))
            ], dim=2)
        # mask out
        pos_beds=pos_beds* \
                Variable(mask.unsqueeze(2).type(torch.FloatTensor),requires_grad=False)
        return pos_beds

    def _fix_enc_hidden(self, h):
        #  the encoder hidden is  (layers*directions) x batch x dim
        #  we need to convert it to layers x batch x (directions*dim)
        if self.encoder.num_directions == 2:
            return h.view(h.size(0) // 2, 2, h.size(1), h.size(2)) \
                    .transpose(1, 2).contiguous() \
                    .view(h.size(0) // 2, h.size(1), h.size(2) * 2)
        else:
            return h

    def compute_embedding(self, input, mask=None):
        batch_size=input.size(0)
        lengths=[int(_x.sum()) for _x in mask]
        enc_hidden, context = self.encoder(input,lengths=lengths) # these are masked
        # 1. make embedding: first hidden, last hidden, maxpool
        fwd_out=enc_hidden[0][-2,:,:]
        bcwd_out=enc_hidden[0][-1,:,:]
        maxpool,_=torch.max(context,dim=1)
        return torch.cat([fwd_out,bcwd_out,maxpool],dim=1)

    def forward(self, input, mask=None):
        batch_size=input.size(0)
        lengths=[int(_x.sum()) for _x in mask]
        enc_hidden, context = self.encoder(input,lengths=lengths) # these are masked
        # 1. make embedding: first hidden, last hidden, maxpool
        fwd_out=enc_hidden[0][-2,:,:]
        bcwd_out=enc_hidden[0][-1,:,:]
        """
        last_out=[]
        for _l,_x in zip(lengths,context.split(1)):
            last_out.append(_x[0,_l-1,:])
        last_out=torch.stack(last_out)
        """
        maxpool,_=torch.max(context,dim=1)
        # 2. make positional embeddings
        pos_embeddings=self.get_positional_embeddings(mask)
        # 3. make sequence input to decoder
        sequence_embedding=torch.cat([fwd_out,bcwd_out,maxpool],dim=1)
        sequence_embedding=sequence_embedding.unsqueeze(1).expand(-1,max(lengths),-1)
        dec_input=torch.cat([sequence_embedding,pos_embeddings],dim=2)
        dec_input=nn.utils.rnn.pack_padded_sequence(dec_input,lengths,batch_first=True)
        # 4. make hidden state of decoder
        h_size = (self.decoder.num_layers, batch_size, self.decoder.hidden_size)
        h_0 = Variable(torch.FloatTensor(*h_size).zero_(), requires_grad=False)
        c_0 = Variable(torch.FloatTensor(*h_size).zero_(), requires_grad=False)
        dec_hidden = (h_0,c_0)
        # 5. feed into decoder
        out, dec_hidden = self.decoder(input, dec_hidden, dec_input, mask)
        # 6. pad output
        mask_var=torch.autograd.Variable(mask.type(torch.FloatTensor).unsqueeze(2), \
                requires_grad=False)
        masked_out=out*mask_var
        return masked_out
