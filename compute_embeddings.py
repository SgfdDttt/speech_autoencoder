# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4
import sys
import time
import os
import math
import filecmp
import subprocess
import argparse
import numpy as np
import torch
import torch.nn as nn
from data_loader import Dataset
from Models import Encoder,Decoder,Autoencoder,custom_init
from torch.autograd import Variable
from torch import optim
import torch.nn.functional as F

try:
    import h5features
except ImportError:
    sys.path.insert(0, os.path.join(
        os.path.dirname(os.path.dirname(os.path.dirname(
            os.path.realpath(__file__)))), 'h5features'))
    import h5features

# argument parser
parser = argparse.ArgumentParser()
parser.add_argument("--datafile",
    help="file containing the input data")
parser.add_argument("--verbose",help="print out some infos",action="store_true")
parser.add_argument("--reload",help="where to reload from",type=str,default=None)
parser.add_argument("--savefile",
    help="file to save the features to",default="features")
args = parser.parse_args()
print("=== ARGUMENTS ===")
print(args)
print("=================")

# load data
data=torch.load(args.datafile)
dataset=Dataset(data)

print('reloading from file ' + args.reload)
model=torch.load(args.reload)

all_files=set()
file2feats=dict()
file2times=dict()
num_samples=0
counter=0
all_segments=dataset.get_all_segments()
num_segments=len(all_segments)
batchsize=1000
for batchi in range(0,len(all_segments),batchsize):
    sp=[_c[0] for _c in all_segments[batchi:batchi+batchsize]]
    fnames=[_c[1] for _c in all_segments[batchi:batchi+batchsize]]
    times=[_c[2] for _c in all_segments[batchi:batchi+batchsize]]
    counter+=len(sp)
    sp_sorted,ordering=dataset.sort_by_length(sp)
    sp_padded,mask=dataset.pad_batch(sp_sorted)
    print(str(counter) + "/" + str(num_segments))
    sys.stdout.flush()
    # forward through model, add artificial batch size of 1
    output=model.compute_embedding(Variable(sp_padded), mask.byte())
    output=output.data.numpy().tolist()
    permutation=dict((_b,_a) for _a,_b in enumerate(ordering[::-1]))
    output_reordered=[]
    for jj in range(len(ordering)):
        output_reordered.append(output[permutation[jj]])
    # save features
    all_files = all_files | set(fnames)
    for _bed,_fname,_time in zip(output_reordered,fnames,times):
        file2feats.setdefault(_fname,[])
        file2times.setdefault(_fname,[])
        file2feats[_fname].append(_bed)
        file2times[_fname].append((_time[0]+_time[1])/2)

# save to hp5 features
features,times=[],[]
files=sorted(list(all_files))
for fname in files:
    permutation=np.argsort(np.array(file2times[fname])) # sort times in increasing sequence
    _times,_features=[],[]
    for ii in permutation:
        _features.append(file2feats[fname][ii])
        _times.append(file2times[fname][ii])
    features.append(np.array(_features))
    times.append(np.array(_times))
    print(features[-1].shape)
    print(times[-1].shape)
    assert features[-1].shape[0]==times[-1].shape[0]

"""
print('files')
print(len(files))
print(files[0])
print('features')
print(len(features))
print(features[0])
print(len(features[0]))
print(len(features[0][0]))
print('times')
print(len(times))
print(times[0])
"""

if os.path.exists(args.savefile):
    os.remove(args.savefile)

print("saving to " + args.savefile)
h5features.write(args.savefile, 'features', files, times, features)
