# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4
# custom data loader to load speech corpus
import sys
import torch
import glob
import math

savefile=sys.argv[1] #'/export/a12/nholzen/phn2ie_data_large.pt'
NORMALIZE=False#True
epsilon=1e-8 # small quantity

def compute_stats(vectors):
    # vectors is a list of torch tensors
    # we just normalize everything without speaker id
    mean=torch.zeros(vectors[0].size(1))
    stddev=torch.zeros(vectors[0].size(1))
    for v in vectors:
        mean+=v.sum(dim=0)
        stddev+=torch.pow(v,2).sum(dim=0)
    num_items=sum(v.size(0) for v in vectors)
    mean/=num_items
    stddev/=num_items
    stddev-=torch.pow(mean, 2)
    stddev=torch.pow(stddev, 0.5)
    return mean, stddev

def normalize(vectors, mean, stddev):
    # vectors is a list of torch tensors
    stddev_reg=stddev+epsilon
    for ii in range(len(vectors)):
        vectors[ii]=torch.add(vectors[ii],-1,mean)
        vectors[ii]=torch.div(vectors[ii],stddev_reg)

def load_data(speech_files):
    # speech is saved with file name followed by one mfcc per line
    speech_data={}
    for fname in speech_files:
        print('  ' + fname)
        id=fname.split('/')[-1].split('.')[0]
        speech_data[id]=[]
        for line in open(fname,'r'):
            frame=line.strip('\n')
            while '  ' in frame:
                frame=frame.replace('  ', ' ')
            frame=[float(c) for c in frame.strip(' ').split(' ')]
            speech_data[id].append(frame)
    return speech_data

def get_segments(speech_data, segment_file):
    sp_data=[]
    segments=[]
    total,failed=0,0
    for line in open(segment_file,'r'):
        fname,start,stop = line.strip('\n').split(' ')[:3]
        if fname not in speech_data:
            continue
        total+=1
        if total%1000==0:
            print(total)
        start_ind=int(math.floor((float(start)-0.0125)*100))
        start_ind=max(start_ind,0)
        stop_ind=int(math.ceil((float(stop)-0.0125)*100))
        stop_ind=min(stop_ind,len(speech_data[fname])-1)
        if start_ind>stop_ind:
            failed+=1
            continue
        sp_data.append(torch.FloatTensor(speech_data[fname][start_ind:stop_ind+1]))
        segments.append((fname,start,stop))
        assert len(sp_data[-1].size())==2, str((fname,start_ind,stop_ind))
        assert sp_data[-1].size(0)>0, str((fname,start,stop))
        assert sp_data[-1].size(1)>0, str((fname,start,stop))
    print("found " + str(total) + " segments, failed on " + str(failed) + " segments")
    assert len(segments)==len(sp_data)
    return sp_data,segments
# end def preprocess

def make_data(src, train_item, valid_item=None):
    print('load data')
    data=load_data(src)
    print('get train segments')
    train_d,segments=get_segments(data, train_item)
    if NORMALIZE:
        mean,stddev=compute_stats(train_d)
        normalize(train_d,mean,stddev)
    if valid_item is not None:
        print('get valid segments')
        valid_d,segments=get_segments(data, valid_item)
        print('saving to ' + str(savefile))
        torch.save({'train': train_d, 'valid': valid_d}, savefile)
    else:
        print('saving to ' + str(savefile))
        torch.save({'data': train_d, 'segments': segments}, savefile)

basedir="/scratch2/jkaradayi/data/challenge_datasets_Aren_features/english_normalized"
train_src=set(glob.glob(basedir+'/*.feat'))
train_item_file='/scratch2/nholzenb/mdu_item.train.item'
dev_item_file='/scratch2/nholzenb/mdu_item.dev.item'

make_data(train_src, train_item_file, dev_item_file)
