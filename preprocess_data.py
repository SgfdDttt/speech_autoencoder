# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4
# custom data loader to load speech corpus
import sys
import torch
import glob
import math

savefile=sys.argv[1] #'/export/a12/nholzen/phn2ie_data_large.pt'
NORMALIZE=False#True
epsilon=1e-8 # small quantity

def compute_stats(vectors):
    # vectors is a list of torch tensors
    # we just normalize everything without speaker id
    mean=torch.zeros(vectors[0].size(1))
    stddev=torch.zeros(vectors[0].size(1))
    for v in vectors:
        mean+=v.sum(dim=0)
        stddev+=torch.pow(v,2).sum(dim=0)
    num_items=sum(v.size(0) for v in vectors)
    mean/=num_items
    stddev/=num_items
    stddev-=torch.pow(mean, 2)
    stddev=torch.pow(stddev, 0.5)
    return mean, stddev

def normalize(vectors, mean, stddev):
    # vectors is a list of torch tensors
    stddev_reg=stddev+epsilon
    for ii in range(len(vectors)):
        vectors[ii]=torch.add(vectors[ii],-1,mean)
        vectors[ii]=torch.div(vectors[ii],stddev_reg)

def preprocess(speech_files, segment_file):
    # speech is saved with file name followed by one mfcc per line
    speech_data={}
    for fname in speech_files:
        print('  ' + fname)
        id=fname.split('/')[-1].split('.')[0]
        speech_data[id]=[]
        for line in open(fname,'r'):
            frame=line.strip('\n')
            while '  ' in frame:
                frame=frame.replace('  ', ' ')
            frame=[float(c) for c in frame.strip(' ').split(' ')]
            speech_data[id].append(frame)
    sp_data=[]
    print('get segments')
    failed=0
    total=0
    for line in open(segment_file,'r'):
        fname,start,stop,_ = line.strip('\n').split(' ')
        if fname not in speech_data:
            continue
        total+=1
        start_ind=int(math.floor((float(start)-0.0125)*100))
        start_ind=max(start_ind,0)
        stop_ind=int(math.ceil((float(stop)-0.0125)*100))
        stop_ind=min(stop_ind,len(speech_data[fname])-1)
        if start_ind>stop_ind:
            failed+=1
            continue
        sp_data.append(torch.FloatTensor(speech_data[fname][start_ind:stop_ind+1]))
        assert len(sp_data[-1].size())==2, str((fname,start_ind,stop_ind))+str(len(speech_data[fname]))
        assert sp_data[-1].size(0)>0, str((fname,start,stop))
        assert sp_data[-1].size(1)>0, str((fname,start,stop))
    print("found " + str(total) + " segments, failed on " + str(failed) + " segments")
    return sp_data
# end def preprocess

def make_data(train_src, valid_src, segment_file):
    print('segment file: ' + segment_file)
    print('train')
    train_d=preprocess(train_src, segment_file)
    if NORMALIZE:
        mean,stddev=compute_stats(train_d)
        normalize(train_d,mean,stddev)
    print('valid')
    valid_d=preprocess(valid_src, segment_file)
    if NORMALIZE:
        normalize(valid_d,mean,stddev)
    print('Saving data to \''+savefile+'\'...')
    save_data = {'train': train_d, 'valid': valid_d}
    torch.save(save_data, savefile)

basedir="/scratch2/jkaradayi/data/challenge_datasets_Aren_features/english_normalized"
valid_src=[
's8713','s6233','s6519',
's1769','s5723','s7460',
's1425','s2517','s5583',
's5093','s8075','s7061',
's4629'] # this list can be found in stats.txt or made from stats.py
valid_src=set(valid_src)
valid_src=set([basedir+'/'+_s+".feat" for _s in valid_src])
train_src=set(glob.glob(basedir+'/*.feat'))-valid_src
segment_file='/scratch2/jkaradayi/data/challenge_datasets_Aren_features/english_ali2.txt'

make_data(train_src, valid_src, segment_file)
